package top.lixxing.http.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;
import top.lixxing.http.client.ws.WebSocketConnection;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class HttpRequesterTest {

    @Test
    public void get() {

        String response = HttpRequester.get("http://www.baidu.com")
                .ignoreSSLCheck(true)
                .doRequestWithResponse();
        System.err.println(response);
    }

    @Test
    public void post() {

        String response = HttpRequester.post("http://www.baidu.com")
                .ignoreSSLCheck(true)
                .doRequestWithResponse();
        System.err.println(response);
    }

    @Test
    public void trace() {

        String response = HttpRequester.trace("http://www.baidu.com")
                .ignoreSSLCheck(true)
                .doRequestWithResponse();
        System.err.println(response);
    }

    @Test
    public void doRequestWithResponse() throws JsonProcessingException {

        String response = HttpRequester.get("https://tools.lixxing.top/api/client-info")
                .doRequestWithResponse();

        System.out.println(response);
    }

    @Test
    public void doRequestWithObject() throws JsonProcessingException {

        Map<String, Object> response = HttpRequester.get("https://tools.lixxing.top/api/client-info")
                .doRequestWithObject(new TypeReference<Map<String, Object>>() {});

        System.out.println(response);
    }

    @Test
    public void doWs() {
        WebSocketConnection connect = HttpRequester.websocket("http://localhost:8787/api/ws")
//                .header("Connection", "Upgrade")
//                .header("Upgrade", "websocket")
//                .header("Sec-WebSocket-Version", "13")
////                .header("Sec-WebSocket-Key", "key==")
//                .header("X-test", "test")
//                .doRequestWithResponse();
                .onOpen(res -> System.out.println(res))
                .onTextMessage(msg -> System.out.println(Thread.currentThread().getName() + ":" + msg))
                .onClose((res) -> System.out.println("连接关闭"))
                .onError(res -> System.out.println(res))
                .connect();

//        connect.onMessage((msg) -> {
//            System.out.println("接收到消息: " + msg);
//        });

        connect.sendMessage("测试");

//        while (true) {
//
//        }
    }

    @Test
    public void testByte() {

        System.out.println("129: " + (byte) 129);
        System.out.println("128: " + (byte) 128);
        System.out.println("130: " + (byte) 130);
        System.out.println("136: " + (byte) 136);
        System.out.println("137: " + (byte) 137);
        System.out.println("138: " + (byte) 138);
    }
}