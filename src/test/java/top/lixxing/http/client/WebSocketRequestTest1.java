package top.lixxing.http.client;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.lixxing.http.client.ws.WebSocketConnection;

import java.util.Date;

public class WebSocketRequestTest1 {

    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(WebSocketRequestTest1.class);

        WebSocketConnection connect = HttpRequester.websocket("http://localhost:8787/api/ws")
//        WebSocketConnection connect = HttpRequester.websocket("https://tools.lixxing.top/api/ws")
                .header("x-client-id", "1111")
                .onOpen(res -> logger.info("code: {} - msg: {} - protocol: {} - headers: {}", res.getCode(), res.getMsg(), res.getProtocol(), res.getHeaders()))
                .onPing(msg -> logger.info("收到ping: {}", msg))
                .onPong(msg -> logger.info("收到pong: {}", msg))
                .onTextMessage(msg -> logger.info("客户端收到消息: {}", msg))
                .onError(res -> res.getException().printStackTrace())
                .onClose(res -> logger.warn("连接关闭"))
                .connect();

        connect.sendMessage("测试");

        new Thread(() -> {
            try {
                while (!connect.isClosed()) {
                    connect.sendMessage("客户端发送消息==>" + new Date());
                    Thread.sleep(10000);
                    connect.ping("客户端ping==>" + new Date());
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                connect.close();
            }
        }).start();

//        connect.close();
//        connect.close(1011, "测试");
//        connect.disConnect();
//        connect.disConnect();
        // 添加jvm退出回调,关闭WebSocket连接
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (connect.readyState().closeable()) {
                logger.warn(connect.readyState().name());
                connect.close();
            }
            logger.warn(connect.readyState().name());
        }, "shutdownHook"));
    }
}