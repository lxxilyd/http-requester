package top.lixxing.http.client;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class HttpSocketTest {

    public static void main(String[] args) {

//        String host = "tools.lixxing.top";
        String host = "localhost";
//        int port = 80;
//        int port = 443;
        int port = 8787;
        Socket client = new Socket();

        try {
//            client = (SSLSocket)((SSLSocketFactory)SSLSocketFactory.getDefault()).createSocket(host, port);

            InetSocketAddress address = new InetSocketAddress(host, port);
            client.connect(address, 20000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (
                final OutputStream outputStream = client.getOutputStream();
                final InputStream inputStream = client.getInputStream()
        ){
            StringBuilder httpContent = new StringBuilder();
            // 请求方法 请求路径 http版本
            httpContent.append("GET ")
//                    .append("/api/client-info ")
                    .append("/api/times ")
                    .append("HTTP/1.1").append("\r\n");
//            httpContent.append("Host: tools.lixxing.top").append("\r\n");
            // 请求头开始
            httpContent.append("Host: localhost").append("\r\n");
            httpContent.append("X-test: test").append("\r\n");
            httpContent.append("\r\n");
            // 请求头结束
            outputStream.write(httpContent.toString().getBytes());
            outputStream.flush();
            Thread.sleep(1000);
            final int available = inputStream.available();
            byte[] data = new byte[available];
            inputStream.read(data);
            System.out.println(new String(data));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
