package top.lixxing.http.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.net.www.http.HttpClient;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class WebSocketRequestTest {

    {
        // 允许设置受限的请求体 例如websocket相关的Sec-开头的请求头
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
    }

    public static void main(String[] args) {

        Logger logger = LoggerFactory.getLogger(WebSocketRequestTest1.class);

        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        HttpURLConnection connection = null;
        HttpRequester.Response.Builder builder = HttpRequester.Response.builder();
        try {
            connection = (HttpURLConnection) new URL("http://localhost:8787/api/ws").openConnection();
            connection.addRequestProperty("Connection", "Upgrade");
            connection.addRequestProperty("Upgrade", "websocket");
            connection.addRequestProperty("Sec-WebSocket-Version", "13");
            connection.addRequestProperty("Sec-WebSocket-Key", "key==");
            connection.setDoOutput(true);
            connection.connect();

            sun.net.www.protocol.http.HttpURLConnection rawConnect = (sun.net.www.protocol.http.HttpURLConnection) connection;

            Field httpField = sun.net.www.protocol.http.HttpURLConnection.class.getDeclaredField("http");
            httpField.setAccessible(true);
            HttpClient client = (HttpClient)httpField.get(rawConnect);


            InputStream inputStream = connection.getInputStream();
            connection.connect();
            new Thread(new ReadMessageHandler(inputStream)).start();
            System.out.println(connection.getResponseCode());
            logger.info(String.valueOf(connection.getResponseCode()));
            System.out.println(connection.getResponseMessage());
            logger.error(connection.getResponseMessage());

            OutputStream outputStream = client.getOutputStream();
//            outputStream.write("{\"message\":\"test\"}".getBytes(StandardCharsets.UTF_8));
//            outputStream.write(new byte[]{0, 9, -27, -113, -111, -23, -128, -127, -26, -107, -80});
//            outputStream.write(new byte[]{-127, 68, -27, -113, -111, -23, -128, -127, -26, -107, -80});
//            byte[] messageBytes = "{\"message\":\"test\"}".getBytes(StandardCharsets.UTF_8);
            String message = "你好";
//            byte[] messageBytes = "{\"message\":\"test\"}".getBytes(StandardCharsets.UTF_8);
            byte[] messageBytes = (message).getBytes(StandardCharsets.UTF_8);
//            outputStream.write(new byte[]{-127, (byte)messageBytes.length});
//            outputStream.write(new byte[]{-120, (byte)messageBytes.length});
            outputStream.write(new byte[]{-119, (byte)messageBytes.length});
//            outputStream.write(new byte[]{-118, (byte)messageBytes.length});
            outputStream.write(messageBytes);
            outputStream.flush();

        } catch (Exception e) {
            if (connection != null) {
                connection.disconnect();
                e.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public static class ReadMessageHandler implements Runnable{

        private final InputStream inputStream;

        public ReadMessageHandler(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            try {
                System.out.println("开始读取数据...");
                int read;
                while ((read = inputStream.read()) != -1) {
                    int length = 0;
                    System.out.println("raw read: " + read);
                    read = (byte)read;
                    System.out.println("read: " + read);
                    if (read == -127) {
                        length = inputStream.read();
                    }
                    if (read == -120) {
                        System.out.println("read: " + read);
                        int available = inputStream.available();
                        byte[] bytes = new byte[available];
                        inputStream.read(bytes);
                        System.out.println("bytes: " + Arrays.toString(bytes));
                    }
                    if (read == -118) {
                        System.out.println("read: " + read);
                        int available = inputStream.available();
                        byte[] bytes = new byte[available];
                        inputStream.read(bytes);
                        System.out.println("bytes: " + Arrays.toString(bytes));
                        System.out.println(new String(bytes));
                    }
                    byte[] buffer = new byte[length];
                    inputStream.read(buffer);
                    if (length > 0) {
                        System.out.println("原始数据: " + Arrays.toString(buffer));
                        System.out.println("读取到数据: " + new String(buffer));
                    }
                }
                System.out.println("read: " + read);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}