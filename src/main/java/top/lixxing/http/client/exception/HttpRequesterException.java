package top.lixxing.http.client.exception;

public class HttpRequesterException extends RuntimeException {

    public HttpRequesterException(String message) {
        super(message);
    }

    public HttpRequesterException(Throwable throwable) {
        super(throwable);
    }

    public HttpRequesterException(String message, Throwable throwable) {
        super(message, throwable);
    }
}