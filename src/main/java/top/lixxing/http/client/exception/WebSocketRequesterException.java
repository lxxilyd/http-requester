package top.lixxing.http.client.exception;

public class WebSocketRequesterException extends RuntimeException {

    public WebSocketRequesterException(String message) {
        super(message);
    }

    public WebSocketRequesterException(Throwable throwable) {
        super(throwable);
    }

    public WebSocketRequesterException(String message, Throwable throwable) {
        super(message, throwable);
    }
}