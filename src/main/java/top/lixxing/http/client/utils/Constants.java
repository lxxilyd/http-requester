package top.lixxing.http.client.utils;

public final class Constants {

    /**
     * 字符集相关
     */
    public static final String CHARSET = "UTF-8";

    /**
     * HTTP相关
     */
    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_PUT = "PUT";
    public static final String METHOD_PATCH = "PATCH";
    public static final String METHOD_DELETE = "DELETE";
    public static final String METHOD_HEAD = "HEAD";
    public static final String METHOD_OPTIONS = "OPTIONS";
    public static final String METHOD_TRACE = "TRACE";
    public static final String METHOD_CONNECT = "CONNECT";
    public static final String CONTENT_TYPE= "Content-Type";
    public static final String CONTENT_LENGTH = "Content-Length";
    public static final String CONNECTION = "Connection";
    public static final String USER_AGENT = "User-Agent";
    public static final String ACCEPT = "*/*";
    public static final String KEEP_ALIVE = "keep-alive";
    public static final String ACCEPT_ENCODING = "gzip, deflate, br";
    public static final String USER_AGENT_VALUE = "HttpRequester/1.0";
    public static final String CONTENT_DISPONSION_WITH_FILE = "Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n";
    public static final String CONTENT_DISPONSION_WITH_KEY = "Content-Disposition: form-data; name=\"%s\"\r\n\r\n";
    public static final String CONTENT_TYPE_WITH_MINE = "Content-Type: %s\r\n\r\n";
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String CONTENT_TYPE_JS = "application/javascript";
    public static final String CONTENT_TYPE_STREAM = "application/octet-stream";
    public static final String CONTENT_TYPE_UNKONW = "content/unknown";
    public static final String CONTENT_TYPE_FORM_URLDECODED = "application/x-www-form-urlencoded";
    public static final String CONTENT_TYPE_FORM_MULITPART = "multipart/form-data; boundary=";
    public static final String TLS = "TLS";
    public static final String CHAR_LIB = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String BOUNDARY_PREFIX = "-----------------------------";
    public static final String BOUNDARY_SUFFIX = "--";
    public static final String NEW_LINE = "\r\n";
    public static final int BOUNDARY_LENGTH = 30;
    public static final int WS_KEY_LENGTH = 15;

    /**
     * Websocket相关
     */
    //                                                  Hex byte int desc
    public final static int WS_KEEP_FLAG = 128;   // 0x0 -128 128 持续帧
    public final static int WS_TEXT_FLAG = 129;   // 0x1 -127 129 文本帧
    public final static int WS_BINARY_FLAG = 130; // 0x2 -126 130 二进制帧

    public final static int WS_DIS_CONNECT_FLAG = -1;  // 0x8 -120 136 连接断开 流关闭
    public final static int WS_CLOSE_FLAG = 136;  // 0x8 -120 136 连接关闭帧
    public final static int WS_PING_FLAG = 137;   // 0x9 -119 137 ping帧 客户端不发送ping 服务端主动发送ping
    public final static int WS_PONG_FLAG = 138;   // 0xA -118 138 pong帧 客户端回应pong

    public final static int WS_CLOSE_CODE_NORMAL = 1000; //自定义关闭正常响应码
    public final static String WS_CLOSE_TEXT_NORMAL = "client closed"; // 自定义关闭正常响应码
    public final static int WS_CLOSE_CODE_DEFAULT = 1006; // 用于期望收到状态码时连接非正常关闭 (也就是说，没有发送关闭帧).
    public final static String WS_CLOSE_TEXT_DEFAULT = ""; // 用于期望收到状态码时连接非正常关闭 (也就是说，没有发送关闭帧).

}
