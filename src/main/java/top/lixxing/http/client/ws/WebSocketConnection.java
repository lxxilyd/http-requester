package top.lixxing.http.client.ws;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static top.lixxing.http.client.utils.Constants.*;

public class WebSocketConnection {

    private final WebSocketRequester requester;

    public WebSocketConnection(WebSocketRequester requester) {
        this.requester = requester;
    }

    /**
     * 向连接发送消息
     * @param message 消息内容
     */
    public void sendMessage(String message) {
        sendMessage(message, StandardCharsets.UTF_8);
    }

    /**
     * 向连接发送消息
     * @param message 消息内容
     * @param charset 消息字符集
     */
    public void sendMessage(String message, Charset charset) {
        requester.sendMessage(message.getBytes(charset), WS_TEXT_FLAG);
    }

    /**
     * 向连接发送二进制消息
     * @param data 消息内容
     */
    public void sendMessage(byte[] data) {
        requester.sendMessage(data, WS_BINARY_FLAG);
    }

    /**
     * 向连接发送ping消息
     */
    public void ping() {
        ping("ping...", StandardCharsets.UTF_8);
    }

    /**
     * 向连接发送ping消息
     * @param message 消息内容
     */
    public void ping(String message) {
        ping(message, StandardCharsets.UTF_8);
    }

    /**
     * 向连接发送ping消息
     * @param message 消息内容
     * @param charset 消息字符集
     */
    public void ping(String message, Charset charset) {
        requester.ping(message.getBytes(charset));
    }

    /**
     * 关闭连接 发送关闭消息
     */
    public void close() {
        requester.close(WS_CLOSE_CODE_DEFAULT, WS_CLOSE_TEXT_DEFAULT);
    }

    /**
     * 关闭连接
     * @param message 响应信息
     */
    public void close(String message) {
        requester.close(WS_CLOSE_CODE_NORMAL, message);
    }

    /**
     * 关闭连接
     * @param code 响应码
     * @param message 响应信息
     */
    public void close(int code, String message) {
        requester.close(code, message);
    }

    /**
     * 获取connection关闭状态
     * @return 关闭状态
     */
    public boolean isClosed() {
        return requester.isClose.get();
    }

    /**
     * 获取WebSocket链接状态
     * @return 链接状态
     */
    public ReadyState readyState() {
        return requester.readyState.get();
    }
}
