package top.lixxing.http.client.ws;

public enum ReadyState {
    CONNECTING(0),
    OPEN(1),
    CLOSING(2),
    CLOSED(3);

    private final int value;

    ReadyState(int value){
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    /**
     * 判断当前状态是否可关闭
     * @return 是否可关闭
     */
    public boolean closeable() {
        return this == CONNECTING || this == OPEN;
    }
}
