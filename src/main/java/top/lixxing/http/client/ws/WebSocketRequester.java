package top.lixxing.http.client.ws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.net.www.http.HttpClient;

import top.lixxing.http.client.HttpRequester;
import top.lixxing.http.client.exception.WebSocketRequesterException;

import static top.lixxing.http.client.utils.Constants.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class WebSocketRequester extends HttpRequester {

    public static final Logger LOGGER = LoggerFactory.getLogger(WebSocketRequester.class);

    private HttpURLConnection connection;

    private Consumer<String> onTextMessage;
    private Consumer<String> onPing;
    private Consumer<String> onPong;
    private Consumer<byte[]> onBinaryMessage;
    private Consumer<Response> onOpen;
    private Consumer<Response> onClose;

    private OutputStream outputStream;

    private Thread handleMessageThread;

    final AtomicBoolean isClose = new AtomicBoolean(true);
    final AtomicReference<ReadyState> readyState = new AtomicReference<>(ReadyState.CLOSED);

    public WebSocketRequester(String url) {
        super(url, METHOD_GET);
    }


    /**
     * 设置请求头
     * @param name 请求头名称
     * @param value 请求头值
     * @return
     */
    @Override
    public WebSocketRequester header(String name, String value) {
        super.header(name, value);
        return this;
    }

    /**
     * pil设置请求头
     * @param headers 请求头
     * @return
     */
    public WebSocketRequester header(Map<String, String> headers) {
        super.header(headers);
        return this;
    }

    /**
     * 设置请求参数(url?${参数名1}=${参数值1}&${参数名2}=${参数值2}....)
     * @param name 参数名称
     * @param value 参数值
     * @return
     */
    public WebSocketRequester param(String name, Object value) {
        super.param(name, value);
        return this;
    }

    /**
     * 批量设置请求参数(url?${参数名1}=${参数值1}&${参数名2}=${参数值2}....)
     * @param params
     * @return
     */
    public WebSocketRequester param(Map<String, Object> params) {
        super.param(params);
        return this;
    }

    /**
     * 设置打开连接回调
     * @param onOpen 回调方法
     * @return WebSocketRequester
     */
    public WebSocketRequester onOpen(Consumer<Response> onOpen) {
        this.onOpen = onOpen;
        return this;
    }

    /**
     * 设置收到文本消息回调
     * @param onTextMessage 回调方法
     * @return WebSocketRequester
     */
    public WebSocketRequester onTextMessage(Consumer<String> onTextMessage) {
        this.onTextMessage = onTextMessage;
        return this;
    }

    /**
     * 设置收到二进制消息回调
     * @param onBinaryMessage 回调方法
     * @return WebSocketRequester
     */
    public WebSocketRequester onBinaryMessage(Consumer<byte[]> onBinaryMessage) {
        this.onBinaryMessage = onBinaryMessage;
        return this;
    }

    /**
     * 设置收到Ping消息回调
     * @param onPing 回调方法
     * @return WebSocketRequester
     */
    public WebSocketRequester onPing(Consumer<String> onPing) {
        this.onPing = onPing;
        return this;
    }

    /**
     * 设置收到Ping消息回调
     * @param onPong 回调方法
     * @return WebSocketRequester
     */
    public WebSocketRequester onPong(Consumer<String> onPong) {
        this.onPong = onPong;
        return this;
    }

    /**
     * 设置连接异常回调
     * @param onError 回调方法
     * @return WebSocketRequester
     */
    @Override
    public WebSocketRequester onError(Consumer<Response> onError) {
        this.error = onError;
        return this;
    }

    /**
     * 设置连接关闭回调
     * @param onClose 回调方法
     * @return WebSocketRequester
     */
    public WebSocketRequester onClose(Consumer<Response> onClose) {
        this.onClose = onClose;
        return this;
    }

    /**
     * 建立websocket连接
     * @return WebSocketConnection对象
     */
    public WebSocketConnection connect() {
        if (!isClose.get()) {
            throw new WebSocketRequesterException("连接已打开,无需重复进行连接操作");
        }
        return doConnect();
    }

    /**
     * 开始建立连接
     * @return WebSocketConnection对象
     */
    private WebSocketConnection doConnect() {
        Response.Builder builder = Response.builder();
        try {
            readyState.set(ReadyState.CONNECTING);
            connection = (HttpURLConnection) prepareUrl(url).openConnection();
            prepareRequest(connection);
            InputStream inputStream = connection.getInputStream();
            outputStream = getOutStream(connection);
            builder.code(connection.getResponseCode())
                    .msg(connection.getResponseMessage())
                    .headers(connection.getHeaderFields())
                    .port(connection.getURL().getPort())
                    .protocol(connection.getURL().getProtocol());
            if (onOpen != null) {
                onOpen.accept(builder.build());
            }
            isClose.set(false);
            readyState.set(ReadyState.OPEN);
            handleMessageThread = new Thread(() -> handleMessage(inputStream), "MessageListener");
            handleMessageThread.start();
            connection.connect();
            return new WebSocketConnection(this);
        } catch (Exception e) {
            isClose.set(true);
            builder.exception(e);
            error.accept(builder.build());
            handleMessageThread.stop();
            if (connection != null) {
                connection.disconnect();
            }
            LOGGER.error("连接失败: {}", e.getMessage(), e);
            return null;
        }
    }

    /**
     * 发送消息
     * @param data 消息数据
     * @param wsFlag 消息格式(WS_TEXT_FLAG 文本消息 WS_BINARY_FLAG 二进制消息)
     */
    void sendMessage(byte[] data, int wsFlag) {
        try {
            outputStream.write(wsFlag); // 消息头 前八位
            outputStream.write(data.length); // 消息长度
            outputStream.write(data); // 消息内容
            outputStream.flush();
        } catch (Exception e) {
            LOGGER.error("发送消息失败: {}", e.getMessage(), e);
        }
    }

    /**
     * 发送消息
     * @param data 消息数据
     */
    void ping(byte[] data) {
        try {
            outputStream.write(WS_PING_FLAG); // 消息头 前八位
            outputStream.write(data.length); // 消息长度
            outputStream.write(data); // 消息内容
            outputStream.flush();
        } catch (Exception e) {
            LOGGER.error("发送ping消息失败: {}", e.getMessage(), e);
        }
    }

    /**
     * 关闭连接
     * @param code 响应码
     * @param message 响应信息
     */
    void close(int code, String message) {
        if (isClose.get()) {
            throw new WebSocketRequesterException("connection is closed");
        }
        try {
            readyState.set(ReadyState.CLOSING);
            byte[] closeData = message.getBytes(StandardCharsets.UTF_8);
            outputStream.write(WS_CLOSE_FLAG); // 消息头 前八位
            outputStream.write(closeData.length + 2); // 消息长度 固定 + 状态码 + 实际消息长度
            outputStream.write(3); // 固定两位二进制 00(0) 01(1) 10(2) 11(3) 11为自定义状态码
            outputStream.write(code); // 状态码
            outputStream.write(closeData);
            outputStream.flush();
            onClose.accept(null);
            isClose.set(true);
            connection.disconnect();
            readyState.set(ReadyState.CLOSED);
        } catch (Exception e) {
            isClose.set(true);
            readyState.set(ReadyState.CLOSED);
            throw new WebSocketRequesterException("关闭连接失败: " + e.getMessage(), e);
        }
    }

    /**
     * 获取真正的输出流
     * @param connection HttpUrlConnection对象
     * @return 输出流
     */
    private OutputStream getOutStream(HttpURLConnection connection) {
        try {
            String protocol = connection.getURL().getProtocol();
            Object rawConnect = connection;
            if ("https".equals(protocol)) {
                Field delegateField = connection.getClass().getDeclaredField("delegate");
                delegateField.setAccessible(true);
                rawConnect = delegateField.get(rawConnect);
            }
            Field httpField = sun.net.www.protocol.http.HttpURLConnection.class.getDeclaredField("http");
            httpField.setAccessible(true);
            HttpClient client = (HttpClient)httpField.get(rawConnect);
            return client.getOutputStream();
        } catch (Exception e) {
            throw new WebSocketRequesterException("获取输出流失败: " + e.getMessage(), e);
        }
    }

    /**
     * 处理服务端消息
     * @param inputStream 输入流
     */
    private void handleMessage(InputStream inputStream) {
        try {
            int read;
            while (!isClose.get()) {
                read = inputStream.read();
                switch (read) {
                    case WS_TEXT_FLAG:
                        handleTextMessage(inputStream);
                        break;
                    case WS_BINARY_FLAG:
                        handleBinaryMessage(inputStream);
                        break;
                    case WS_PING_FLAG:
                        handlePing(inputStream);
                        break;
                    case WS_PONG_FLAG:
                        handlePong(inputStream);
                        break;
                    case WS_CLOSE_FLAG:
                    case WS_DIS_CONNECT_FLAG:
                        handleClose(inputStream);
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            isClose.set(true);
            readyState.set(ReadyState.CLOSED);
            LOGGER.error("处理消息失败: {}", e.getMessage(), e);
        }
    }

    /**
     * 处理文本消息
     * @param inputStream 输入流
     * @throws IOException IO异常
     */
    private void handleTextMessage(InputStream inputStream) throws IOException {
        int length = inputStream.read();
        byte[] data = new byte[length];
        inputStream.read(data);
        callback(onTextMessage, new String(data));
    }

    /**
     * 处理二进制消息
     * @param inputStream 输入流
     * @throws IOException IO异常
     */
    private void handleBinaryMessage(InputStream inputStream) throws IOException {
        int length = inputStream.read();
        byte[] data = new byte[length];
        inputStream.read(data);
        callback(onBinaryMessage, data);
    }

    /**
     * 处理ping心跳消息
     * @param inputStream 输入流
     * @throws IOException IO异常
     */
    private void handlePing(InputStream inputStream) throws IOException {
        int length = inputStream.read();
        byte[] data = new byte[length];
        inputStream.read(data);
        LOGGER.info("收到服务端ping心跳: {}" , new String(data));
        LOGGER.info("开始向服务端回应pong消息...");
        callback(onPing, new String(data, StandardCharsets.UTF_8));
        sendMessage(data, WS_PONG_FLAG);
    }

    /**
     * 处理pong心跳消息
     * @param inputStream 输入流
     * @throws IOException IO异常
     */
    private void handlePong(InputStream inputStream) throws IOException {
        int length = inputStream.read();
        byte[] data = new byte[length];
        inputStream.read(data);
        LOGGER.info("收到服务端pong心跳: {}" , new String(data));
        callback(onPong, new String(data, StandardCharsets.UTF_8));
    }

    /**
     * 处理关闭消息
     * @param inputStream 输入流
     * @throws IOException IO异常
     */
    private void handleClose(InputStream inputStream) throws IOException {
        LOGGER.info("断开连接...{}", inputStream.available());
        isClose.set(true);
        readyState.set(ReadyState.CLOSED);
        callback(onClose, null);
    }

    /**
     * 回调
     * @param consumer 回调方法
     * @param param 回调参数
     * @param <T> 回调类型参数
     */
    private <T> void callback(Consumer<T> consumer, T param) {
        if (consumer != null) {
            consumer.accept(param);
        }
    }
}
